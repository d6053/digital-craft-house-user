package rabbitmq

import (
	amqp "github.com/streadway/amqp"
)

func EstablishConn(url string) (c *amqp.Channel, err error) {
	if url == "" {
		url = "amqp://localhost"
	}

	connection, err := amqp.Dial(url)
	if err != nil {
		return nil, err
	}

	ch, err := connection.Channel()

	if err != nil {
		return nil, err
	}

	return ch, nil
}

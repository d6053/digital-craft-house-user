package rabbitmq

import "context"

type RabbitMQService interface {
	ListenForUsers(ctx context.Context)
}

package rabbitmq

import (
	"context"
	"digital-craft-house-user/src/domain/seller"
	"encoding/json"
	"fmt"
	"log"
)

type rabbitmqService struct {
	service seller.SellerService
	url     string
	// username string
	// password string
	// hostname string
	// port     string
}

func NewRabbitMQService(service seller.SellerService, url string) RabbitMQService {
	return &rabbitmqService{
		service,
		url,
		// username,
		// password,
		// hostname,
		// port,
	}
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}

func (s *rabbitmqService) ListenForUsers(ctx context.Context) {
	ch, err := EstablishConn(s.url)
	if err != nil {
		failOnError(err, "Failed to establish connection with rabbit mq")
	}

	q, err := ch.QueueDeclare(
		"CreateUser", // name
		false,        // durable
		false,        // delete when unused
		false,        // exclusive
		false,        // no-wait
		nil,          // arguments
	)
	failOnError(err, "Failed to declare a queue")

	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	failOnError(err, "Failed to register a consumer")

	var forever chan struct{}

	go func() {
		for d := range msgs {
			log.Printf("Received a message: %s", d.Body)

			var params seller.CreateSellerParams
			err := json.Unmarshal(d.Body, &params)
			if err != nil {
				fmt.Println("Can't unmarshal the byte array")
				return
			}

			seller, err := s.service.CreateSeller(ctx, params)
			if err != nil {
				failOnError(err, "Failed to register a seller")
			}
			fmt.Println(seller)
		}
	}()

	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever
}

package middleware

import (
	"digital-craft-house-user/src/grpc/protogo/token"
	"net/http"
)

func ValidateToken(client *token.TokenGrpcClientImpl) func(http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		// A HTTP handler is a function.
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			req := r
			tokenCookie, err := r.Cookie("access_token")
			if err != nil {
				http.Error(w, "No cookie in the request", http.StatusUnauthorized)
				return
			}

			tokenRequest := token.TokenRequest{}
			tokenRequest.TokenString = tokenCookie.Value
			tokenRequest.Referer = ""

			token, err := client.ValidateToken(r.Context(), &tokenRequest)
			if err != nil {
				http.Error(w, err.Error(), http.StatusUnauthorized)
				return // don't call original handler
			}

			if !token.Valid {
				http.Error(w, "Token is invalid", http.StatusUnauthorized)
				return // don't call original handler
			}

			// Call initial handler.
			h.ServeHTTP(w, req)
		})
	}
}

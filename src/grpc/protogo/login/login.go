package login

import (
	"digital-craft-house-user/src/domain/seller"
	"digital-craft-house-user/src/hashing"
	"strings"

	"golang.org/x/net/context"
)

type Server struct {
	Pepper     string
	Repository seller.SellerRepository
}

func (s *Server) GetUserByCredentials(c context.Context, r *LoginRequest) (*LoginResponse, error) {

	cus, err := s.Repository.GetSellerByEmail(c, r.Email)
	if err != nil {
		return nil, err
	}

	saltAndPassword := cus.Password

	salt := strings.Split(saltAndPassword, "$")[0]

	if hashing.CheckPassword(saltAndPassword, r.Password, []byte(salt), []byte(s.Pepper)) {

		seller, err := s.Repository.GetSellerByCredentials(c, r.Email, saltAndPassword)
		if err != nil {
			return nil, err
		}

		response := LoginResponse{
			Uuid:  seller.UUID,
			Email: seller.Email,
		}
		return &response, nil
	}
	response := LoginResponse{
		Uuid:  "",
		Email: "",
	}
	return &response, nil
}

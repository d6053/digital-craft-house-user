package token

import (
	context "context"
	"log"

	grpc "google.golang.org/grpc"
)

type TokenGrpcClientImpl struct {
	grpcAuthKey string
	client      TokenServiceClient
}

func NewTokenGrpcClient(host, grpcAuthKey string) (*TokenGrpcClientImpl, error) {
	conn, err := grpc.Dial(host, grpc.WithInsecure())
	if err != nil {
		return nil, err
	}

	client := NewTokenServiceClient(conn)

	return &TokenGrpcClientImpl{
		grpcAuthKey: grpcAuthKey,
		client:      client,
	}, nil
}

func (c *TokenGrpcClientImpl) ValidateToken(ctx context.Context, in *TokenRequest, opts ...grpc.CallOption) (*TokenResponse, error) {
	response, err := c.client.ValidateToken(ctx, in)
	if err != nil {
		log.Fatalf("Error: %s", err)
	}
	return response, nil
}

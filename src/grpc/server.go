package grpc

import (
	"fmt"
	"log"
	"net"

	"digital-craft-house-user/src/domain/seller"
	"digital-craft-house-user/src/grpc/protogo/login"

	"google.golang.org/grpc"
)

func GrpcServer(r seller.SellerRepository, host string, port string, p string) {
	lis, err := net.Listen("tcp", fmt.Sprintf("%s:%s", host, port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := login.Server{
		Pepper:     p,
		Repository: r,
	}

	grpcServer := grpc.NewServer()

	login.RegisterLoginServiceServer(grpcServer, &s)

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %s", err)
	}
}

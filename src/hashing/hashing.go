package hashing

import (
	"crypto/sha512"
	"encoding/base64"
	"math/rand"
	"time"
)

// Define salt size
const SaltSize = 16

const charset = "abcdefghijklmnopqrstuvwxyz" +
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#%^&*()_=-"

var seededRand *rand.Rand = rand.New(
	rand.NewSource(time.Now().UnixNano()))

func GenerateRandomSalt(saltSize int) string {
	b := make([]byte, saltSize)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

// Generate 16 bytes randomly and securely using the
// Cryptographically secure pseudorandom number generator (CSPRNG)
// in the crypto.rand package
// func GenerateRandomSalt(saltSize int) []byte {
// 	var salt = make([]byte, saltSize)

// 	_, err := rand.Read(salt[:])

// 	if err != nil {
// 		panic(err)
// 	}

// 	return salt
// }

// Combine password and salt then hash them using the SHA-512
// hashing algorithm and then return the hashed password
// as a base64 encoded string
func HashPassword(password string, salt []byte, pepper []byte) string {
	// Convert password string to byte slice
	var passwordBytes = []byte(password)

	// Create sha-512 hasher
	var sha512Hasher = sha512.New()

	// Append salt to password
	passwordBytes = append(passwordBytes, salt...)
	passwordBytes = append(passwordBytes, pepper...)

	// Write password bytes to the hasher
	sha512Hasher.Write(passwordBytes)

	// Get the SHA-512 hashed password
	var hashedPasswordBytes = sha512Hasher.Sum(nil)

	// Convert the hashed password to a base64 encoded string
	var base64EncodedPasswordHash = base64.URLEncoding.EncodeToString(hashedPasswordBytes)

	return base64EncodedPasswordHash
}

// Check if two passwords match
func CheckPassword(hashedPassword, currPassword string, salt []byte, pepper []byte) bool {
	var currPasswordHash = HashPassword(currPassword, salt, pepper)

	finalOutput := string(salt) + "$" + string(currPasswordHash)
	return hashedPassword == finalOutput
}

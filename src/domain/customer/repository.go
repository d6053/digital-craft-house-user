package customer

import (
	"context"
)

type CustomerRepository interface {
	ListCustomers(ctx context.Context) (CustomerCollection, error)
	GetCustomerByUuid(ctx context.Context, uuid string) (Customer, error)
	GetCustomerByCredentials(ctx context.Context, email string, password string) (Customer, error)
	GetCustomerByEmail(ctx context.Context, email string) (Customer, error)

	CreateCustomer(ctx context.Context, arg CreateCustomerParams) (*Customer, error)
	UpdateCustomer(ctx context.Context, arg UpdateCustomerParams, uuid string) (*Customer, error)

	DeleteCustomer(ctx context.Context, uuid string) error
}

package customer

import (
	"context"
	"database/sql"
	"digital-craft-house-user/src/hashing"
	"time"

	"github.com/google/uuid"
)

type customerService struct {
	pepper     string
	repository CustomerRepository
}

// NewCustomer returns the customer service implementation.
func NewCustomerService(pepper string, repository CustomerRepository) CustomerService {
	return &customerService{
		pepper,
		repository,
	}
}

// List all stored customer
func (s *customerService) ListCustomers(ctx context.Context) (res CustomerCollection, err error) {
	customers, err := s.repository.ListCustomers(ctx)
	if err != nil {
		return nil, err
	}

	return customers, nil
}

// Show customer by ID
func (s *customerService) GetCustomerByUuid(ctx context.Context, uuid string) (res *Customer, err error) {
	customer, err := s.repository.GetCustomerByUuid(ctx, uuid)
	if err != nil {
		return nil, err
	}
	return &customer, nil
}

// Add new cusotmer and return its uuid.
func (s *customerService) CreateCustomer(ctx context.Context, c *CreateCustomerParams) (*Customer, error) {
	uuid := uuid.New().String()

	// First generate random 16 byte salt
	var salt = hashing.GenerateRandomSalt(hashing.SaltSize)

	// Hash password using the salt and pepper
	var hashedPassword = hashing.HashPassword(c.Password, []byte(salt), []byte(s.pepper))

	//Store both salt and hashed password in db
	storeInDB := string(salt) + "$" + string(hashedPassword)

	params := CreateCustomerParams{
		UUID:      uuid,
		Email:     c.Email,
		Password:  storeInDB,
		FirstName: c.FirstName,
		LastName:  c.LastName,
		CreatedAt: time.Now(),
		isAdmin:   c.isAdmin,
	}
	customer, err := s.repository.CreateCustomer(ctx, params)
	if err != nil {
		if err == sql.ErrNoRows {
			return customer, nil
		}
		return nil, err
	}
	return customer, nil
}

// Add new cusotmer and return its uuid.
func (s *customerService) UpdateCustomer(ctx context.Context, c *UpdateCustomerParams, uuid string) (*Customer, error) {
	params := UpdateCustomerParams{
		Email:     c.Email,
		FirstName: c.FirstName,
		LastName:  c.LastName,
	}

	customer, err := s.repository.UpdateCustomer(ctx, params, uuid)
	if err != nil {
		if err == sql.ErrNoRows {
			return customer, nil
		}
		return nil, err
	}
	return customer, nil
}

// Remove customer from storage
func (s *customerService) DeleteCustomer(ctx context.Context, uuid string) (err error) {
	err = s.repository.DeleteCustomer(ctx, uuid)
	if err != nil {
		return err
	}
	return nil
}

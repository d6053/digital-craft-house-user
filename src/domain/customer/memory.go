package customer

import (
	"context"
	"database/sql"
	repository "digital-craft-house-user/src/storage/mysql"
	"digital-craft-house-user/src/storage/mysql/migration"
	query "digital-craft-house-user/src/storage/mysql/query"
	"digital-craft-house-user/src/util"
	"errors"
	"net/mail"

	"github.com/go-sql-driver/mysql"
)

type customerRepository struct {
	queries *repository.Queries
}

func NewCustomerRepository(ctx context.Context, db *sql.DB) (CustomerRepository, error) {
	if _, err := db.ExecContext(ctx, migration.CreateCustomerTable); err != nil {
		return nil, err
	}
	return &customerRepository{
		queries: repository.NewQueries(db),
	}, nil
}

func (repository *customerRepository) ListCustomers(ctx context.Context) (CustomerCollection, error) {
	rows, err := repository.queries.DB.QueryContext(ctx, query.ListCustomers)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	customers := CustomerCollection{}
	for rows.Next() {
		var c Customer
		if err := rows.Scan(
			&c.UUID,
			&c.Email,
			&c.Password,
			&c.FirstName,
			&c.LastName,
			&c.CreatedAt,
			&c.isAdmin,
		); err != nil {
			return nil, err
		}
		customers = append(customers, &c)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return customers, nil
}

func (repository *customerRepository) GetCustomerByUuid(ctx context.Context, uuid string) (Customer, error) {
	row := repository.queries.DB.QueryRowContext(ctx, query.ShowCustomer, uuid)
	var c Customer
	err := row.Scan(
		&c.UUID,
		&c.Email,
		&c.Password,
		&c.FirstName,
		&c.LastName,
		&c.CreatedAt,
		&c.isAdmin,
	)
	return c, err
}

func (repository *customerRepository) CreateCustomer(ctx context.Context, arg CreateCustomerParams) (*Customer, error) {
	_, err := mail.ParseAddress(arg.Email)
	if err != nil {
		return nil, err
	}

	row := repository.queries.DB.QueryRowContext(ctx, query.CreateCustomer,
		&arg.UUID,
		&arg.Email,
		&arg.Password,
		&arg.FirstName,
		&arg.LastName,
		&arg.CreatedAt,
		&arg.isAdmin,
	)

	c := new(Customer)
	err = row.Scan(
		&c.UUID,
		&c.Email,
		&c.Password,
		&c.FirstName,
		&c.LastName,
		&c.CreatedAt,
		&c.isAdmin,
	)
	if err != nil {
		var mysqlErr *mysql.MySQLError
		if errors.As(err, &mysqlErr) && mysqlErr.Number == 1062 {
			err := util.NewErrorf(util.ErrorStatusInternal, util.ErrorCodeInternal, "Customer with this email already exists")
			return nil, err
		}
	}
	return c, err
}

func (repository *customerRepository) UpdateCustomer(ctx context.Context, arg UpdateCustomerParams, uuid string) (*Customer, error) {
	_, err := mail.ParseAddress(arg.Email)
	if err != nil {
		return nil, err
	}

	row := repository.queries.DB.QueryRowContext(ctx, query.UpdateCustomer,
		&arg.Email,
		&arg.FirstName,
		&arg.LastName,
		uuid,
	)

	c := new(Customer)
	err = row.Scan(
		&c.Email,
		&c.FirstName,
		&c.LastName,
	)
	if err != nil {
		var mysqlErr *mysql.MySQLError
		if errors.As(err, &mysqlErr) && mysqlErr.Number == 1062 {
			err := util.NewErrorf(util.ErrorStatusInternal, util.ErrorCodeInternal, "Customer with this email already exists")
			return nil, err
		}
	}
	return c, err
}

func (repository *customerRepository) DeleteCustomer(ctx context.Context, uuid string) error {
	_, err := repository.queries.DB.ExecContext(ctx, query.DeleteCustomer, uuid)
	return err
}

func (repository *customerRepository) GetCustomerByCredentials(ctx context.Context, email string, password string) (Customer, error) {

	row := repository.queries.DB.QueryRowContext(ctx, query.GetCustomerByCredentials, email, password)

	var c Customer
	err := row.Scan(
		&c.UUID,
		&c.Email,
		&c.Password,
		&c.FirstName,
		&c.LastName,
		&c.CreatedAt,
		&c.isAdmin,
	)

	return c, err
}

func (repository *customerRepository) GetCustomerByEmail(ctx context.Context, email string) (Customer, error) {

	row := repository.queries.DB.QueryRowContext(ctx, query.GetCustomerByEmail, email)

	var c Customer
	err := row.Scan(
		&c.UUID,
		&c.Email,
		&c.Password,
		&c.FirstName,
		&c.LastName,
		&c.CreatedAt,
		&c.isAdmin,
	)

	return c, err
}

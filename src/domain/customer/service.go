package customer

import "context"

type CustomerService interface {
	ListCustomers(ctx context.Context) (CustomerCollection, error)
	CreateCustomer(ctx context.Context, c *CreateCustomerParams) (*Customer, error)
	UpdateCustomer(ctx context.Context, c *UpdateCustomerParams, uuid string) (*Customer, error)

	GetCustomerByUuid(ctx context.Context, uuid string) (*Customer, error)
	DeleteCustomer(ctx context.Context, uuid string) error
}

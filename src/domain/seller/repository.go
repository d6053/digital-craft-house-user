package seller

import (
	"context"
)

type SellerRepository interface {
	ListSellers(ctx context.Context) (SellerCollection, error)
	GetSellerByUuid(ctx context.Context, uuid string) (Seller, error)
	GetSellerByCredentials(ctx context.Context, email string, password string) (Seller, error)
	GetSellerByEmail(ctx context.Context, email string) (Seller, error)
	CreateSeller(ctx context.Context, arg CreateSellerParams) (*Seller, error)
	UpdateSeller(ctx context.Context, arg UpdateSellerParams, uuid string) (*Seller, error)
	DeleteSeller(ctx context.Context, uuid string) error
}

package seller

import "time"

type Seller struct {
	UUID        string    `json:"uuid"`
	Email       string    `json:"email"`
	Password    string    `json:"password"`
	FirstName   string    `json:"first_name"`
	LastName    string    `json:"last_name"`
	Location    string    `json:"location"`
	PostCode    string    `json:"post_code"`
	PhoneNumber string    `json:"phone_number"`
	Description string    `json:"description"`
	CreatedAt   time.Time `json:"created_at"`
	isAdmin     bool      `json:"is_admin"`
}
type CreateSellerParams struct {
	UUID        string    `json:"uuid"`
	Email       string    `json:"email"`
	Password    string    `json:"password"`
	FirstName   string    `json:"first_name"`
	LastName    string    `json:"last_name"`
	Location    string    `json:"location"`
	PostCode    string    `json:"post_code"`
	PhoneNumber string    `json:"phone_number"`
	Description string    `json:"description"`
	CreatedAt   time.Time `json:"created_at"`
	isAdmin     bool      `json:"is_admin"`
}
type UpdateSellerParams struct {
	Email       string `json:"email"`
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
	Location    string `json:"location"`
	PostCode    string `json:"post_code"`
	PhoneNumber string `json:"phone_number"`
	Description string `json:"description"`
}

type SellerCollection []*Seller

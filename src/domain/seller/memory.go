package seller

import (
	"context"
	"database/sql"
	repository "digital-craft-house-user/src/storage/mysql"
	"digital-craft-house-user/src/storage/mysql/migration"
	query "digital-craft-house-user/src/storage/mysql/query"
	"digital-craft-house-user/src/util"
	"errors"
	"net/mail"

	"github.com/go-sql-driver/mysql"
)

type sellerRepository struct {
	queries *repository.Queries
}

func NewSellerRepository(ctx context.Context, db *sql.DB) (SellerRepository, error) {
	if _, err := db.ExecContext(ctx, migration.CreateSellerTable); err != nil {
		return nil, err
	}
	return &sellerRepository{
		queries: repository.NewQueries(db),
	}, nil
}

func (repository *sellerRepository) ListSellers(ctx context.Context) (SellerCollection, error) {
	rows, err := repository.queries.DB.QueryContext(ctx, query.ListSellers)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	sellers := SellerCollection{}
	for rows.Next() {
		var s Seller
		if err := rows.Scan(
			&s.UUID,
			&s.Email,
			&s.Password,
			&s.FirstName,
			&s.LastName,
			&s.Location,
			&s.PostCode,
			&s.PhoneNumber,
			&s.Description,
			&s.CreatedAt,
			&s.isAdmin,
		); err != nil {
			return nil, err
		}
		sellers = append(sellers, &s)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return sellers, nil
}

func (repository *sellerRepository) GetSellerByUuid(ctx context.Context, uuid string) (Seller, error) {
	row := repository.queries.DB.QueryRowContext(ctx, query.ShowSeller, uuid)
	var s Seller
	err := row.Scan(
		&s.UUID,
		&s.Email,
		&s.Password,
		&s.FirstName,
		&s.LastName,
		&s.Location,
		&s.PostCode,
		&s.PhoneNumber,
		&s.Description,
		&s.CreatedAt,
		&s.isAdmin,
	)
	return s, err
}

func (repository *sellerRepository) CreateSeller(ctx context.Context, arg CreateSellerParams) (*Seller, error) {
	_, err := mail.ParseAddress(arg.Email)
	if err != nil {
		return nil, err
	}

	row := repository.queries.DB.QueryRowContext(ctx, query.CreateSeller,
		&arg.UUID,
		&arg.Email,
		&arg.Password,
		&arg.FirstName,
		&arg.LastName,
		&arg.Location,
		&arg.PostCode,
		&arg.PhoneNumber,
		&arg.Description,
		&arg.CreatedAt,
		&arg.isAdmin,
	)

	s := new(Seller)

	err = row.Scan(
		&s.UUID,
		&s.Email,
		&s.Password,
		&s.FirstName,
		&s.LastName,
		&s.Location,
		&s.PostCode,
		&s.PhoneNumber,
		&s.Description,
		&s.CreatedAt,
		&s.isAdmin,
	)

	if err != nil {

		var mysqlErr *mysql.MySQLError
		if errors.As(err, &mysqlErr) && mysqlErr.Number == 1062 {
			err := util.NewErrorf(util.ErrorStatusInternal, util.ErrorCodeInternal, "Seller with this email already exists")
			return nil, err
		}

	}
	return s, err
}

func (repository *sellerRepository) UpdateSeller(ctx context.Context, arg UpdateSellerParams, uuid string) (*Seller, error) {
	_, err := mail.ParseAddress(arg.Email)
	if err != nil {
		return nil, err
	}

	row := repository.queries.DB.QueryRowContext(ctx, query.UpdateSeller,
		&arg.Email,
		&arg.FirstName,
		&arg.LastName,
		&arg.Location,
		&arg.PostCode,
		&arg.PhoneNumber,
		&arg.Description,
		uuid,
	)

	s := new(Seller)

	err = row.Scan(
		&s.Email,
		&s.FirstName,
		&s.LastName,
		&s.Location,
		&s.PostCode,
		&s.PhoneNumber,
		&s.Description,
	)

	if err != nil {

		var mysqlErr *mysql.MySQLError
		if errors.As(err, &mysqlErr) && mysqlErr.Number == 1062 {
			err := util.NewErrorf(util.ErrorStatusInternal, util.ErrorCodeInternal, "Seller with this email already exists")
			return nil, err
		}

	}
	return s, err
}

func (repository *sellerRepository) DeleteSeller(ctx context.Context, uuid string) error {
	_, err := repository.queries.DB.ExecContext(ctx, query.DeleteSeller, uuid)
	return err
}

func (repository *sellerRepository) GetSellerByCredentials(ctx context.Context, email string, password string) (Seller, error) {

	row := repository.queries.DB.QueryRowContext(ctx, query.GetSellerByCredentials, email, password)

	var s Seller
	err := row.Scan(
		&s.UUID,
		&s.Email,
		&s.Password,
		&s.FirstName,
		&s.LastName,
		&s.Location,
		&s.PostCode,
		&s.PhoneNumber,
		&s.Description,
		&s.CreatedAt,
		&s.isAdmin,
	)

	return s, err
}

func (repository *sellerRepository) GetSellerByEmail(ctx context.Context, email string) (Seller, error) {

	row := repository.queries.DB.QueryRowContext(ctx, query.GetSellerByEmail, email)

	var s Seller
	err := row.Scan(
		&s.UUID,
		&s.Email,
		&s.Password,
		&s.FirstName,
		&s.LastName,
		&s.Location,
		&s.PostCode,
		&s.PhoneNumber,
		&s.Description,
		&s.CreatedAt,
		&s.isAdmin,
	)

	return s, err
}

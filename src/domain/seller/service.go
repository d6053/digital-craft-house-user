package seller

import (
	"context"
)

type SellerService interface {
	ListSellers(ctx context.Context) (SellerCollection, error)
	GetSellerByUuid(ctx context.Context, uuid string) (*Seller, error)
	CreateSeller(ctx context.Context, arg CreateSellerParams) (*Seller, error)
	UpdateSeller(ctx context.Context, arg UpdateSellerParams, uuid string) (*Seller, error)
	DeleteSeller(ctx context.Context, uuid string) error
}

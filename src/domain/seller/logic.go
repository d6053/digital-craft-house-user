package seller

import (
	"context"
	"database/sql"
	"digital-craft-house-user/src/hashing"

	"github.com/google/uuid"
)

// seller service example implementation.
// The example methods log the requests and return zero values.
type sellerService struct {
	pepper     string
	repository SellerRepository
}

// NewSeller returns the seller service implementation.
func NewSellerService(pepper string, repository SellerRepository) SellerService {
	return &sellerService{
		pepper,
		repository,
	}
}

// List all stored sellers
func (s *sellerService) ListSellers(ctx context.Context) (SellerCollection, error) {
	sellers, err := s.repository.ListSellers(ctx)
	if err != nil {
		return nil, err
	}

	return sellers, nil
}

// Show seller by ID
func (s *sellerService) GetSellerByUuid(ctx context.Context, uuid string) (*Seller, error) {
	seller, err := s.repository.GetSellerByUuid(ctx, uuid)
	if err != nil {
		return nil, err
	}
	return &seller, nil
}

// Add new seller and return its uuid.
func (s *sellerService) CreateSeller(ctx context.Context, c CreateSellerParams) (*Seller, error) {
	uuid := uuid.New().String()

	// First generate random 16 byte salt
	var salt = hashing.GenerateRandomSalt(hashing.SaltSize)

	// Hash password using the salt and pepper
	var hashedPassword = hashing.HashPassword(c.Password, []byte(salt), []byte(s.pepper))

	//Store both salt and hashed password in db
	storeInDB := string(salt) + "$" + string(hashedPassword)

	params := CreateSellerParams{
		UUID:        uuid,
		Email:       c.Email,
		Password:    storeInDB,
		FirstName:   c.FirstName,
		LastName:    c.LastName,
		Location:    c.Location,
		PostCode:    c.PostCode,
		PhoneNumber: c.PhoneNumber,
		Description: c.Description,
		CreatedAt:   c.CreatedAt,
	}

	seller, err := s.repository.CreateSeller(ctx, params)
	if err != nil {
		if err == sql.ErrNoRows {
			return seller, nil
		}
		return nil, err
	}
	return seller, nil
}

func (s *sellerService) UpdateSeller(ctx context.Context, arg UpdateSellerParams, uuid string) (*Seller, error) {
	params := UpdateSellerParams{
		Email:       arg.Email,
		FirstName:   arg.FirstName,
		LastName:    arg.LastName,
		Location:    arg.Location,
		PostCode:    arg.PostCode,
		PhoneNumber: arg.PhoneNumber,
	}

	seller, err := s.repository.UpdateSeller(ctx, params, uuid)
	if err != nil {
		if err == sql.ErrNoRows {
			return seller, nil
		}
		return nil, err
	}
	return seller, nil
}

// Update existing seller and return its uuid.
func (s *sellerService) Update(ctx context.Context, seller Seller) (res string, err error) {
	return
}

// Remove seller from storage
func (s *sellerService) DeleteSeller(ctx context.Context, uuid string) (err error) {
	err = s.repository.DeleteSeller(ctx, uuid)
	if err != nil {
		return err
	}
	return nil
}

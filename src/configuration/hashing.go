package config

import "os"

type Hashing struct {
	Pepper string
}

func LoadHashingEnv() Hashing {
	hashing := Hashing{
		Pepper: os.Getenv("PEPPER"),
	}

	return hashing
}

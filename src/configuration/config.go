package config

import (
	"os"

	"github.com/joho/godotenv"
)

type Config struct {
	Application Application
	MySQLDB     MySQLDB
	Grpc        Grpc
	Http        Http
	Hashing     Hashing
	RabbitMQ    RabbitMQ
}

func LoadEnv() (*Config, error) {
	env := os.Getenv("APP_ENV")
	if env != "production" && env != "develop" {
		err := godotenv.Load()
		if err != nil {
			return nil, err
		}
	}

	application := LoadApplicationEnv()
	mySQLDB := LoadMySQLDBEnv()
	grpc := LoadGrpcEnv()
	hashing := LoadHashingEnv()
	http := LoadHTTPEnv()
	rabbitmq := LoadRabbitMQEnv()

	conf := Config{
		Application: application,
		MySQLDB:     mySQLDB,
		Grpc:        grpc,
		Http:        http,
		Hashing:     hashing,
		RabbitMQ:    rabbitmq,
	}

	return &conf, nil
}

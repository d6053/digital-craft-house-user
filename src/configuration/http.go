package config

import "os"

type Http struct {
	Port           string
	AllowedOrigins string
}

func LoadHTTPEnv() Http {
	http := Http{
		Port:           os.Getenv("PORT"),
		AllowedOrigins: os.Getenv("http://localhost:8000"),
	}

	return http
}

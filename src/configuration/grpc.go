package config

import "os"

type Grpc struct {
	ServerHost string
	ClientHost string
	ServerPort string
	ClientPort string
	AuthKey    string
}

func LoadGrpcEnv() Grpc {
	grpc := Grpc{
		ServerHost: os.Getenv("GRPC_SERVER_HOST"),
		ClientHost: os.Getenv("GRPC_CLIENT_HOST"),
		ServerPort: os.Getenv("GRPC_SERVER_PORT"),
		ClientPort: os.Getenv("GRPC_CLIENT_PORT"),
		AuthKey:    os.Getenv("AUTH_KEY"),
	}

	return grpc
}

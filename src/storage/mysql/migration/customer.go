package migration

const CreateCustomerTable = "CREATE TABLE IF NOT EXISTS customer (uuid VARCHAR(255) PRIMARY KEY, email VARCHAR(255) NOT NULL UNIQUE, password VARCHAR(255) NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, created_at TIMESTAMP, is_admin BOOL);"

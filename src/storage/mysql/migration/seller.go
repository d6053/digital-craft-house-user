package migration

const CreateSellerTable = "CREATE TABLE IF NOT EXISTS seller (uuid VARCHAR(255) PRIMARY KEY, email VARCHAR(255) NOT NULL UNIQUE, password VARCHAR(255) NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, location VARCHAR(255), post_code VARCHAR(255), phone_number VARCHAR(255), description VARCHAR(255), created_at TIMESTAMP, is_admin BOOL);"

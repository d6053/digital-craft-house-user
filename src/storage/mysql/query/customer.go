package query

const ListCustomers = "SELECT * FROM `customer`"

const ShowCustomer = "SELECT * FROM `customer` WHERE uuid=?"

const GetCustomerByCredentials = "SELECT * FROM `customer` WHERE email=? AND password=?"

const GetCustomerByEmail = "SELECT * FROM `customer` WHERE email=?"

const CreateCustomer = "INSERT INTO `customer` (uuid, email, password, first_name, last_name, created_at, is_admin) VALUES (?, ?, ?, ?, ?, ?, ?);"

const UpdateCustomer = "UPDATE `customer` SET  email=?, first_name=?, last_name=? WHERE uuid=?;"

const DeleteCustomer = "DELETE FROM `customer` WHERE uuid = ?"

package query

const ListSellers = "SELECT * FROM `seller`"

const ShowSeller = "SELECT * FROM `seller` WHERE uuid=?"

const GetSellerByCredentials = "SELECT * FROM `seller` WHERE email=? AND password=?"

const GetSellerByEmail = "SELECT * FROM `seller` WHERE email=?"

const CreateSeller = "INSERT INTO `seller` (uuid, email, password, first_name, last_name, location, post_code, phone_number, description, created_at, is_admin) VALUES (?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?);"

const UpdateSeller = "UPDATE `seller` SET email=?, first_name=?, last_name=?, location=?, post_code=?, phone_number=?, description=? WHERE uuid=?"

const DeleteSeller = "DELETE FROM `seller` WHERE uuid = ?"

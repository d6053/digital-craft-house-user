package mysql

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

func DSN(username string, password string, hostname string, dbName string) string {
	return fmt.Sprintf("%s:%s@tcp(%s:3306)/%s?allowNativePasswords=true",
		username,
		password,
		hostname,
		dbName)
}

func InitializeMySqlDB(username string, password string, hostname string, dbName string) (*sql.DB, error) {

	// rootCertPool := x509.NewCertPool()
	// pem, _ := ioutil.ReadFile("./BaltimoreCyberTrustRoot.crt.pem")
	// if ok := rootCertPool.AppendCertsFromPEM(pem); !ok {
	// 	log.Fatal("Failed to append PEM.")
	// }
	// mysql.RegisterTLSConfig("custom", &tls.Config{RootCAs: rootCertPool})

	db, err := sql.Open("mysql", DSN(username, password, hostname, dbName)+"&parseTime=true")

	// db, err := sql.Open("mysql", DSN(username, password, hostname, dbName))
	if err != nil {
		log.Printf("Error %s when opening DB\n", err)
		return nil, err
	}
	defer db.Close()

	ctx, cancelfunc := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelfunc()
	res, err := db.ExecContext(ctx, "CREATE DATABASE IF NOT EXISTS "+dbName)
	if err != nil {
		log.Printf("Error %s when creating DB\n", err)
		return nil, err
	}
	no, err := res.RowsAffected()
	if err != nil {
		log.Printf("Error %s when fetching rows", err)
		return nil, err
	}
	log.Printf("rows affected %d\n", no)

	db.Close()
	db, err = sql.Open("mysql", DSN(username, password, hostname, dbName)+"&parseTime=true")
	// db, err = sql.Open("mysql", DSN(username, password, hostname, dbName))

	if err != nil {
		log.Printf("Error %s when opening DB", err)
		return nil, err
	}

	db.SetMaxOpenConns(20)
	db.SetMaxIdleConns(20)
	db.SetConnMaxLifetime(time.Minute * 5)

	ctx, cancelfunc = context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelfunc()
	err = db.PingContext(ctx)
	if err != nil {
		log.Printf("Errors %s pinging DB", err)
		return nil, err
	}
	log.Printf("Connected to DB %s successfully\n", dbName)
	log.Printf("DB running")

	return db, nil
}

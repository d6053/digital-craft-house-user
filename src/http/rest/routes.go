package rest

func (s *server) routes() {

	customers := s.Router.PathPrefix("/customers").Subrouter()
	sellers := s.Router.PathPrefix("/sellers").Subrouter()

	customers.HandleFunc("", s.CreateCustomer).Methods("POST")
	customers.HandleFunc("", s.ListCustomers).Methods("GET")
	customers.HandleFunc("/{uuid}", s.UpdateCustomer).Methods("PUT")
	customers.HandleFunc("/{uuid}", s.GetCustomerByUuid).Methods("GET")
	customers.HandleFunc("/{uuid}", s.DeleteCustomer).Methods("DELETE")

	sellers.HandleFunc("", s.CreateSeller).Methods("POST")
	sellers.HandleFunc("", s.ListSellers).Methods("GET")
	sellers.HandleFunc("/{uuid}", s.UpdateSeller).Methods("PUT")
	sellers.HandleFunc("/{uuid}", s.GetSellerByUuid).Methods("GET")
	sellers.HandleFunc("/{uuid}", s.DeleteSeller).Methods("DELETE")
}

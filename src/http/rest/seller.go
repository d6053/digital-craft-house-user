package rest

import (
	"digital-craft-house-user/src/domain/seller"
	"digital-craft-house-user/src/util"

	"encoding/json"
	"net/http"

	"github.com/go-playground/validator"
	"github.com/gorilla/mux"
)

func (s *server) CreateSeller(w http.ResponseWriter, r *http.Request) {
	var request seller.CreateSellerParams

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInvalid, util.ErrorCodeInvalid, err.Error())
		return
	}

	defer r.Body.Close()

	if err := s.Validator.Struct(request); err != nil {
		errors := err.(validator.ValidationErrors)
		renderErrorResponse(r.Context(), w, util.ErrorStatusInvalid, util.ErrorCodeInvalid, errors.Error())
		return
	}

	seller, err := s.SellerService.CreateSeller(r.Context(), request)
	if err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInternal, util.ErrorCodeInternal, err.Error())
		return
	}

	renderResponse(w, http.StatusCreated, seller)
}

func (s *server) UpdateSeller(w http.ResponseWriter, r *http.Request) {
	var request seller.UpdateSellerParams

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInvalid, util.ErrorCodeInvalid, err.Error())
		return
	}

	defer r.Body.Close()

	if err := s.Validator.Struct(request); err != nil {
		errors := err.(validator.ValidationErrors)
		renderErrorResponse(r.Context(), w, util.ErrorStatusInvalid, util.ErrorCodeInvalid, errors.Error())
		return
	}

	vars := mux.Vars(r)

	seller, err := s.SellerService.UpdateSeller(r.Context(), request, vars["uuid"])
	if err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInternal, util.ErrorCodeInternal, err.Error())
		return
	}

	renderResponse(w, http.StatusCreated, seller)
}

func (s *server) GetSellerByUuid(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	seller, err := s.SellerService.GetSellerByUuid(r.Context(), vars["uuid"])
	if err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInternal, util.ErrorCodeInternal, err.Error())
		return
	}

	renderResponse(w, http.StatusOK, seller)
}

func (s *server) ListSellers(w http.ResponseWriter, r *http.Request) {
	sellers, err := s.SellerService.ListSellers(r.Context())
	if err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInternal, util.ErrorCodeInternal, err.Error())
		return
	}

	renderResponse(w, http.StatusOK, sellers)
}

func (s *server) DeleteSeller(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	err := s.SellerService.DeleteSeller(r.Context(), vars["uuid"])
	if err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInternal, util.ErrorCodeInternal, err.Error())
		return
	}

	renderResponse(w, http.StatusOK, err)
}

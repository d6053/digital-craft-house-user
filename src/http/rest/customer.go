package rest

import (
	"digital-craft-house-user/src/domain/customer"
	"digital-craft-house-user/src/util"

	"encoding/json"
	"net/http"

	"github.com/go-playground/validator"
	"github.com/gorilla/mux"
)

func (s *server) CreateCustomer(w http.ResponseWriter, r *http.Request) {
	var request customer.CreateCustomerParams

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInvalid, util.ErrorCodeInvalid, err.Error())
		return
	}

	defer r.Body.Close()

	if err := s.Validator.Struct(request); err != nil {
		errors := err.(validator.ValidationErrors)
		renderErrorResponse(r.Context(), w, util.ErrorStatusInvalid, util.ErrorCodeInvalid, errors.Error())
		return
	}

	customer, err := s.CustomerService.CreateCustomer(r.Context(), &request)
	if err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInternal, util.ErrorCodeInternal, err.Error())
		return
	}

	renderResponse(w, http.StatusCreated, customer)
}

func (s *server) GetCustomerByUuid(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	customer, err := s.CustomerService.GetCustomerByUuid(r.Context(), vars["uuid"])
	if err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInternal, util.ErrorCodeInternal, err.Error())
		return
	}

	renderResponse(w, http.StatusOK, customer)
}

func (s *server) ListCustomers(w http.ResponseWriter, r *http.Request) {
	customers, err := s.CustomerService.ListCustomers(r.Context())
	if err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInternal, util.ErrorCodeInternal, err.Error())
		return
	}

	renderResponse(w, http.StatusOK, customers)
}

func (s *server) UpdateCustomer(w http.ResponseWriter, r *http.Request) {
	var request customer.UpdateCustomerParams

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInvalid, util.ErrorCodeInvalid, err.Error())
		return
	}

	defer r.Body.Close()

	if err := s.Validator.Struct(request); err != nil {
		errors := err.(validator.ValidationErrors)
		renderErrorResponse(r.Context(), w, util.ErrorStatusInvalid, util.ErrorCodeInvalid, errors.Error())
		return
	}

	vars := mux.Vars(r)

	customer, err := s.CustomerService.UpdateCustomer(r.Context(), &request, vars["uuid"])
	if err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInternal, util.ErrorCodeInternal, err.Error())
		return
	}

	renderResponse(w, http.StatusCreated, customer)
}

func (s *server) DeleteCustomer(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	err := s.CustomerService.DeleteCustomer(r.Context(), vars["uuid"])
	if err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInternal, util.ErrorCodeInternal, err.Error())
		return
	}

	renderResponse(w, http.StatusOK, err)
}

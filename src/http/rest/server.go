package rest

import (
	"context"
	"database/sql"
	"digital-craft-house-user/src/domain/customer"
	"digital-craft-house-user/src/domain/seller"
	"digital-craft-house-user/src/grpc"
	"digital-craft-house-user/src/grpc/protogo/middleware"
	"digital-craft-house-user/src/grpc/protogo/token"
	"digital-craft-house-user/src/rabbitmq"
	"digital-craft-house-user/src/util"

	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/go-playground/validator"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

type server struct {
	Server    *http.Server
	Router    *mux.Router
	Validator *validator.Validate

	CustomerService customer.CustomerService
	SellerService   seller.SellerService
	RabbitMQService rabbitmq.RabbitMQService
}

func NewServer(port string, tokenClient *token.TokenGrpcClientImpl) *server {
	address := ":" + port

	svr := &server{
		Server: &http.Server{
			Addr:         address,
			WriteTimeout: 15 * time.Second,
			ReadTimeout:  15 * time.Second,
		},
		Router: mux.NewRouter(),
	}

	svr.Router.NotFoundHandler = http.HandlerFunc(handleNotFound)

	svr.Router.Use(middleware.ValidateToken(tokenClient))

	svr.routes()

	return svr
}

func (s *server) Init(ctx context.Context, db *sql.DB, pepper string, grpcHost string, grpcServer string, rabbitUrl string) {
	s.Validator = validator.New()

	customerRepository, err := customer.NewCustomerRepository(ctx, db)
	if err != nil {
		fmt.Println("Customer repo can not be initialized")
	}

	sellerRepository, err := seller.NewSellerRepository(ctx, db)
	if err != nil {
		fmt.Println("Seller repo can not be initialized")
	}

	s.CustomerService = customer.NewCustomerService(pepper, customerRepository)
	s.SellerService = seller.NewSellerService(pepper, sellerRepository)
	s.RabbitMQService = rabbitmq.NewRabbitMQService(s.SellerService, rabbitUrl)

	go grpc.GrpcServer(sellerRepository, grpcHost, grpcServer, pepper)
	grpcInfo := fmt.Sprintf("gprc server runs: %s:%s", grpcHost, grpcServer)
	log.Println(grpcInfo)

	go s.RabbitMQService.ListenForUsers(ctx)
	log.Println("rabit mq run ")

}

func (s *server) Run(allowedOrigins string) {

	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"http://localhost:3000", "http://localhost:9000", "http://localhost:9005", "http://localhost:9006"},
		AllowedMethods:   []string{http.MethodGet, http.MethodPost, http.MethodDelete, http.MethodPut},
		AllowedHeaders:   []string{"Content-Type", "AccessToken", "X-CSRF-Token", "Authorization", "Token"},
		AllowCredentials: true,
	})

	s.Server.Handler = c.Handler(s.Router)

	if err := s.Server.ListenAndServe(); err != nil {
		log.Println(err)
	}
}

func handleNotFound(w http.ResponseWriter, r *http.Request) {
	err := util.NewErrorf(util.ErrorStatusNotFound, util.ErrorCodeNotFound, "Endpoint was not found.")
	renderErrorResponse(r.Context(), w, util.ErrorStatusNotFound, util.ErrorCodeNotFound, err.Error())
}

module digital-craft-house-user

go 1.16

require (
	github.com/go-playground/universal-translator v0.18.0 // indirect
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/go-sql-driver/mysql v1.6.0
	github.com/google/uuid v1.3.0
	github.com/gorilla/mux v1.8.0
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0
	github.com/joho/godotenv v1.4.0
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/matryer/is v1.4.0
	github.com/rs/cors v1.8.2
	github.com/streadway/amqp v1.0.0 // indirect
	goa.design/goa/v3 v3.5.5
	golang.org/x/net v0.0.0-20220127200216-cd36cc0744dd
	golang.org/x/sys v0.0.0-20220227234510-4e6760a101f9 // indirect
	google.golang.org/grpc v1.44.0
	google.golang.org/protobuf v1.27.1
)

package main

import (
	"context"
	config "digital-craft-house-user/src/configuration"
	"digital-craft-house-user/src/grpc/protogo/token"
	"digital-craft-house-user/src/http/rest"
	"digital-craft-house-user/src/storage/mysql"
	"fmt"

	"log"
)

func main() {
	ctx := context.Background()
	//Load variables from .env file
	config, err := config.LoadEnv()
	if err != nil {
		log.Fatalf(err.Error())
	}

	//Establish DB connection
	db, err := mysql.InitializeMySqlDB(config.MySQLDB.Username, config.MySQLDB.Password, config.MySQLDB.Host, config.MySQLDB.Name)
	if err != nil {
		log.Println(err)
		log.Fatalf(err.Error())
	}

	addr := fmt.Sprintf("%s:%s", config.Grpc.ClientHost, config.Grpc.ClientPort)

	tokenGrpcClient, err := token.NewTokenGrpcClient(addr, config.Grpc.AuthKey)
	if err != nil {
		log.Fatalf(err.Error())
	}

	svr := rest.NewServer(config.Http.Port, tokenGrpcClient)

	svr.Init(ctx, db, config.Hashing.Pepper, config.Grpc.ServerHost, config.Grpc.ServerPort, config.RabbitMQ.Url)

	svr.Run(config.Http.AllowedOrigins)
}
